import java.util.List;

public class ClientFactory {
		
	List<String[]> clientL;
		   
		   public ClientFactory(List<String[]> clientL)
		   {
			   this.clientL = clientL;
		   }

		   public Client getClient(String name){
			   
			   for(String[] s:clientL)
			   {
				   if (s[0].equalsIgnoreCase(name)){
			            return new RealClient(name);
			         }
			   }
		      return new NullClient();
		   }

}

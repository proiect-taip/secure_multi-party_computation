
public class UserDecorator {
	protected User decoratedUser;

	   public UserDecorator(User decoratedUser){
	      this.decoratedUser = decoratedUser;
	   }

	   public void action(){
	      decoratedUser.action();
	   }	

}

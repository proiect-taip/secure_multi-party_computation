import java.util.List;

public interface Criteria {
	public List<Client> meetCriteria(List<Client> persons);
}

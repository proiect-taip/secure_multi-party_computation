
public class StatisticMaker {

		private Statistic bar;
		private Statistic pie;
		private Statistic scatter;
		
		public StatisticMaker() {
			bar = new StatisticBar(1);
			pie = new StatisticPie(1);
			scatter = new StatisticScatter(1);
		}
		
		public void drawBar(){
			bar.show();
		}
		
		public void drawPie() {
			pie.show();
		}
		
		public void drawScatter(){
			scatter.show();
		}
}

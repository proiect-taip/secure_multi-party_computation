
public class StatisticBar implements Statistic {
	int data;
	
	public StatisticBar(int data)
	{
		this.data=data;
	}
	
	@Override
	public void show() {
		System.out.println("Bar chart with data: "+ this.data);	
	}

}

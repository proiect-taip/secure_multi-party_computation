
public class StatisticPie implements Statistic {
	int data;
	
	public StatisticPie(int data)
	{
		this.data=data;
	}
	
	@Override
	public void show() {
		System.out.println("Pie chart with data: "+this.data);	
		
	}

}

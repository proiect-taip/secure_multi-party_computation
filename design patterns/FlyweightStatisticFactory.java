import java.util.HashMap;

public class FlyweightStatisticFactory {
	private static final HashMap<Integer, Statistic> statisticMap = new HashMap();

	   public static Statistic getStatistic(int data) {
	      Statistic statistic = (Statistic)statisticMap.get(data);

	      if(statistic == null) {
	         statistic = new StatisticBar(data);
	         statisticMap.put(data, statistic);
	         System.out.println("Creating statistc with data : " + data);
	      }
	      return statistic;
	   }
}

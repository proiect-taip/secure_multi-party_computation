
public class StatisticFactory {
	
	public Statistic getTypeByName(String name)
	{
		switch(name){
			case "bar": return new StatisticBar(1); 
			case "pie": return new StatisticPie(1);
			case "scatter": return new StatisticScatter(1);
			default: return null;
		}
		
	}

}

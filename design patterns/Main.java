import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Main {
	public static void main (String [] args)
	{
		User user1 = new User("u1","p1");
		user1.login();
		StatisticBar s = new StatisticBar(1);
		user1.showStatistic(s);
		user1.logout();
		
		//builder
		User.UserBuilder ub = new User.UserBuilder("u2","p2");
		User user2 = ub.createUser();
		user2.login();
		user2.logout();
		
		System.out.println();
		
		//factory
		StatisticFactory sf = new StatisticFactory();
		
		Statistic stat1 = sf.getTypeByName("scatter");
		stat1.show();
		
		Statistic stat2 = sf.getTypeByName("pie");
		stat2.show();
		
		Statistic stat3 = sf.getTypeByName("bar");
		stat3.show();
		
		System.out.println();
		
		//filter
		List<Client> clientList = new ArrayList<Client>();
		clientList.add(new Client("Mike", "Male"));
		clientList.add(new Client("Bobby", "Male"));

		Criteria male = new MaleCriteria();
		Criteria female = new FemaleCriteria();

		System.out.println("number of males: "+male.meetCriteria(clientList).size());
		System.out.println("number of females: "+female.meetCriteria(clientList).size());
		
		System.out.println();
		
		//flyweight
		Statistic statistic1 = (Statistic)FlyweightStatisticFactory.getStatistic(1);
        statistic1.show();
        
        Statistic statistic2 = (Statistic)FlyweightStatisticFactory.getStatistic(1);
        statistic2.show();
        
        Statistic statistic3 = (Statistic)FlyweightStatisticFactory.getStatistic(2);
        statistic3.show();
        
        System.out.println();
        
        //decorator
        UserDecorator admin = new AdministratorDecorator(new User("admin","admin"));
        admin.action();
        
        System.out.println();
        
        //strategy
        Context context = new Context(new DataBaseMessage());		
        context.display();
        context = new Context(new StatisticCriteriaMessage());		
        context.display();
        
        System.out.println();
        
        //facade
        
        StatisticMaker statisticMaker = new StatisticMaker();
        statisticMaker.drawPie();
        statisticMaker.drawScatter();
        statisticMaker.drawBar();
        
        System.out.println();
        
        //null object
        String [] cl1 = {"Client 1", "male"};
        String [] cl2 = {"Client 2", "male"};
        
        List<String []> cLL = new ArrayList<String []>();
        
        cLL.add(cl1);
        cLL.add(cl2);
        
        ClientFactory clients = new ClientFactory(cLL);
        Client client1 = clients.getClient("Client 1");
        Client client2 = clients.getClient("Client 3");

        System.out.println(client1.getName());
        System.out.println(client2.getName());


	}	

}

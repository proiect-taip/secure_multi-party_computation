
public class User {
	
	private String pass;
	public String name;
	
	public User(String name, String pass)
	{
		this.name=name;
		this.pass=pass;
		System.out.println("User "+this.name+ " was created");
	}
	
	public static class UserBuilder
	{
		String p;
		String n;
		public UserBuilder(String name, String pass)
		{
			setName(name);
			setPass(pass);
		}
		
		public User createUser()
		{
			return new User(n,p);
		}
		public void setName(String name)
		{
			n=name;
		}
		
		public void setPass(String pass)
		{
			p=pass;
		}
	}
	
	public void login()
	{
		System.out.println("User "+this.name+ " has logged in");
	}
	
	public void showStatistic(Statistic s)
	{
		System.out.println("User "+this.name+" actions:");
		s.show();
	}
	
	public void action()
	{
		System.out.println("User "+this.name+" action");
	}
	
	public void logout()
	{
		System.out.println("User "+this.name+ " has logged out\n");
	}

}

public class Context {
   private Message msg;

   public Context(Message strategy){
      this.msg = strategy;
   }

   public void display(){
       msg.showMessage();
   }
}
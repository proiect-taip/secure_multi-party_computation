
public class StatisticScatter implements Statistic {
	int data;
	
	public StatisticScatter(int data)
	{
		this.data=data;
	}
	
	@Override
	public void show() {
		System.out.println("Scatter chart with data: "+this.data);	
		
	}

}


public class AdministratorDecorator extends UserDecorator{
	
	public AdministratorDecorator(User decoratedUser) {
	      super(decoratedUser);		
	   }

	   @Override
	   public void action() {
	      decoratedUser.action();	       
	      setAdminRights(decoratedUser);
	   }

	   private void setAdminRights(User decoratedUser){
	      System.out.println("User "+ decoratedUser.name+" has administrator rights");
	   }

}

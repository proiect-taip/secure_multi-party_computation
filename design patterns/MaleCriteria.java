import java.util.ArrayList;
import java.util.List;

public class MaleCriteria implements Criteria {

	@Override
	   public List<Client> meetCriteria(List<Client> clients) {
	      List<Client> maleClients = new ArrayList<Client>(); 
	      
	      for (Client c : clients) {
	         if(c.getGender().equalsIgnoreCase("male")){
	            maleClients.add(c);
	         }
	      }
	      return maleClients;
	   }
}

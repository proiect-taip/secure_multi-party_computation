import java.util.ArrayList;
import java.util.List;

public class FemaleCriteria implements Criteria {

	
	@Override
	   public List<Client> meetCriteria(List<Client> clients) {
	      List<Client> femaleClients = new ArrayList<Client>(); 
	      
	      for (Client c : clients) {
	         if(c.getGender().equalsIgnoreCase("female")){
	            femaleClients.add(c);
	         }
	      }
	      return femaleClients;
	   }

}

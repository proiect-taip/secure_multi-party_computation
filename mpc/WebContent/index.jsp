<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="style.css">
<title>MPC</title>
</head>
<body>
	<div class="container">
		<form class="login" method="post" action="home">
			<fieldset>
				<legend>Login</legend>
				<input type="text" id="name" name="name" placeholder="Username"
					required><br /> <input type="password" id="pass"
					name="pass" placeholder="Password" required><br />
				<div style="color: #FF0000;">${err1}</div>
				<input type="submit" value="Login">
			</fieldset>
		</form>

		<form class="register" method="post" action="register">
			<fieldset>
				<legend>Register</legend>
				<input type="text" id="name" name="name" placeholder="Username"
					required><br /> <input type="password" id="pass"
					name="pass" placeholder="Password" required><br /> <input
					type="radio" name="admin" value="Admin">Admin <input
					type="radio" name="admin" value="Not Admin">Not Admin<br />
				<div style="color: #FF0000;">${err2}</div>
				<input type="submit" value="Register">
			</fieldset>
		</form>
	</div>
</body>
</html>
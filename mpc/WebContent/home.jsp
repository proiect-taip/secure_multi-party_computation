<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home</title>
</head>
<body>
	<div>Session number: ${sessionScope.accessCount}</div>
	<button onclick="displayChart(${sessionScope.chart})">Show Statistics</button>
	<div id='charts'>
		<div id='piechart'></div>
		<div id='chart_div'></div>
		<div id='barchart_material'></div>
	</div>
	<script type='text/javascript' src='https://www.google.com/jsapi'></script>
	<script type='text/javascript' src='script.js'></script>
</body>
</html>
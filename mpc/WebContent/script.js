google.load("visualization", "1", {packages:["corechart"]});
google.load("visualization", "1.1", {packages:["bar"]});

function submit()
{
	var name = document.getElementById("name");
	var pass = document.getElementById("pass");

	var msg = {
			name: name,
			pass: pass,
	};

	var xmlhttp = new XMLHttpRequest();
	xmlhttp.open("POST", "login.jsp", true);
	xmlhttp.setRequestHeader("Content-Type","application/json");
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) 
		{
			document.getElementById("result").innerHTML = xmlhttp.responseText;
		}
	}

	var data = JSON.stringify(msg);
	xmlhttp.send(data);
}

function displayChart(data){
	drawPie(data);
	drawScatter(data);
	drawBar(data);
}



function drawScatter(data) {

    var data1 = google.visualization.arrayToDataTable(data);

    var options = {
            title: 'Diseases vs. Number of people',
            hAxis: {title: 'Diseases', minValue: 0, maxValue: 15},
            vAxis: {title: 'Number of people', minValue: 0, maxValue: 15},
            legend: 'none'
          };

          var chart = new google.visualization.ScatterChart(document.getElementById('chart_div'));

          chart.draw(data1, options);
    
  }

function drawPie(data) {

    var data1 = google.visualization.arrayToDataTable(data);

    var options = {
      title: 'Diseases'
    };

    var chart = new google.visualization.PieChart(document.getElementById('piechart'));

    chart.draw(data1, options);

    
  }

function drawBar(data) {
	var data1 = google.visualization.arrayToDataTable(data);

    var options = {
      chart: {
        title: 'Diseases vs Number of people',
      },
      bars: 'horizontal' // Required for Material Bar Charts.
    };

    var chart = new google.charts.Bar(document.getElementById('barchart_material'));

    chart.draw(data1, options);
  }
	
	


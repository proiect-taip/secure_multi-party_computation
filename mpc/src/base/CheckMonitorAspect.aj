package base;

import java.util.*;
import javamoprt.*;

class CheckMonitor_Set implements javamoprt.MOPSet {
	protected CheckMonitor[] elementData;
	public int size;

	public CheckMonitor_Set(){
		this.size = 0;
		this.elementData = new CheckMonitor[4];
	}

	public final int size(){
		while(size > 0 && elementData[size-1].MOP_terminated) {
			elementData[--size] = null;
		}
		return size;
	}

	public final boolean add(MOPMonitor e){
		ensureCapacity();
		elementData[size++] = (CheckMonitor)e;
		return true;
	}

	public final void endObject(int idnum){
		for(int i = 0; i < size; i++){
			MOPMonitor monitor = elementData[i];
			if(!monitor.MOP_terminated){
				monitor.endObject(idnum);
			}
		}
	}

	public final boolean alive(){
		for(int i = 0; i < size; i++){
			MOPMonitor monitor = elementData[i];
			if(!monitor.MOP_terminated){
				return true;
			}
		}
		return false;
	}

	public final void endObjectAndClean(int idnum){
		for(int i = size - 1; i > 0; i--){
			MOPMonitor monitor = elementData[i];
			if(monitor != null && !monitor.MOP_terminated){
				monitor.endObject(idnum);
			}
			elementData[i] = null;
		}
		elementData = null;
	}

	public final void ensureCapacity() {
		int oldCapacity = elementData.length;
		if (size + 1 > oldCapacity) {
			cleanup();
		}
		if (size + 1 > oldCapacity) {
			Object oldData[] = elementData;
			int newCapacity = (oldCapacity * 3) / 2 + 1;
			if (newCapacity < size + 1){
				newCapacity = size + 1;
			}
			elementData = Arrays.copyOf(elementData, newCapacity);
		}
	}

	public final void cleanup() {
		int num_terminated_monitors = 0 ;
		for(int i = 0; i + num_terminated_monitors < size; i ++){
			CheckMonitor monitor = (CheckMonitor)elementData[i + num_terminated_monitors];
			if(monitor.MOP_terminated){
				if(i + num_terminated_monitors + 1 < size){
					do{
						monitor = (CheckMonitor)elementData[i + (++num_terminated_monitors)];
					} while(monitor.MOP_terminated && i + num_terminated_monitors + 1 < size);
					if(monitor.MOP_terminated){
						num_terminated_monitors++;
						break;
					}
				} else {
					num_terminated_monitors++;
					break;
				}
			}
			if(num_terminated_monitors != 0){
				elementData[i] = monitor;
			}
		}
		if(num_terminated_monitors != 0){
			size -= num_terminated_monitors;
			for(int i = size; i < size + num_terminated_monitors ; i++){
				elementData[i] = null;
			}
		}
	}

	public final void event_getUser(ServletLogin sl) {
		int num_terminated_monitors = 0 ;
		for(int i = 0; i + num_terminated_monitors < this.size; i ++){
			CheckMonitor monitor = (CheckMonitor)this.elementData[i + num_terminated_monitors];
			if(monitor.MOP_terminated){
				if(i + num_terminated_monitors + 1 < this.size){
					do{
						monitor = (CheckMonitor)this.elementData[i + (++num_terminated_monitors)];
					} while(monitor.MOP_terminated && i + num_terminated_monitors + 1 < this.size);
					if(monitor.MOP_terminated){
						num_terminated_monitors++;
						break;
					}
				} else {
					num_terminated_monitors++;
					break;
				}
			}
			if(num_terminated_monitors != 0){
				this.elementData[i] = monitor;
			}
			monitor.Prop_1_event_getUser(sl);
			if(monitor.Prop_1_Category_fail) {
				monitor.Prop_1_handler_fail(sl);
			}
		}
		if(num_terminated_monitors != 0){
			this.size -= num_terminated_monitors;
			for(int i = this.size; i < this.size + num_terminated_monitors; i++){
				this.elementData[i] = null;
			}
		}
	}

	public final void event_check(ServletLogin sl) {
		int num_terminated_monitors = 0 ;
		for(int i = 0; i + num_terminated_monitors < this.size; i ++){
			CheckMonitor monitor = (CheckMonitor)this.elementData[i + num_terminated_monitors];
			if(monitor.MOP_terminated){
				if(i + num_terminated_monitors + 1 < this.size){
					do{
						monitor = (CheckMonitor)this.elementData[i + (++num_terminated_monitors)];
					} while(monitor.MOP_terminated && i + num_terminated_monitors + 1 < this.size);
					if(monitor.MOP_terminated){
						num_terminated_monitors++;
						break;
					}
				} else {
					num_terminated_monitors++;
					break;
				}
			}
			if(num_terminated_monitors != 0){
				this.elementData[i] = monitor;
			}
			monitor.Prop_1_event_check(sl);
			if(monitor.Prop_1_Category_fail) {
				monitor.Prop_1_handler_fail(sl);
			}
		}
		if(num_terminated_monitors != 0){
			this.size -= num_terminated_monitors;
			for(int i = this.size; i < this.size + num_terminated_monitors; i++){
				this.elementData[i] = null;
			}
		}
	}
}

class CheckMonitor extends javamoprt.MOPMonitor implements Cloneable, javamoprt.MOPObject {
	public Object clone() {
		try {
			CheckMonitor ret = (CheckMonitor) super.clone();
			return ret;
		}
		catch (CloneNotSupportedException e) {
			throw new InternalError(e.toString());
		}
	}

	int Prop_1_state;
	static final int Prop_1_transition_getUser[] = {1, 1, 2};;
	static final int Prop_1_transition_check[] = {2, 0, 2};;

	boolean Prop_1_Category_fail = false;

	public CheckMonitor () {
		Prop_1_state = 0;

	}

	public final void Prop_1_event_getUser(ServletLogin sl) {
		MOP_lastevent = 0;

		Prop_1_state = Prop_1_transition_getUser[Prop_1_state];
		Prop_1_Category_fail = Prop_1_state == 2;
	}

	public final void Prop_1_event_check(ServletLogin sl) {
		MOP_lastevent = 1;

		Prop_1_state = Prop_1_transition_check[Prop_1_state];
		Prop_1_Category_fail = Prop_1_state == 2;
	}

	public final void Prop_1_handler_fail (ServletLogin sl){
		{
			System.out.println("Error: check called before getUser!");
		}

	}

	public final void reset() {
		MOP_lastevent = -1;
		Prop_1_state = 0;
		Prop_1_Category_fail = false;
	}

	public javamoprt.MOPWeakReference MOPRef_sl;

	//alive_parameters_0 = [ServletLogin sl]
	public boolean alive_parameters_0 = true;

	public final void endObject(int idnum){
		switch(idnum){
			case 0:
			alive_parameters_0 = false;
			break;
		}
		switch(MOP_lastevent) {
			case -1:
			return;
			case 0:
			//getUser
			//alive_sl
			if(!(alive_parameters_0)){
				MOP_terminated = true;
				return;
			}
			break;

			case 1:
			//check
			//alive_sl
			if(!(alive_parameters_0)){
				MOP_terminated = true;
				return;
			}
			break;

		}
		return;
	}

}

public aspect CheckMonitorAspect implements javamoprt.MOPObject {
	javamoprt.MOPMapManager CheckMapManager;
	public CheckMonitorAspect(){
		CheckMapManager = new javamoprt.MOPMapManager();
		CheckMapManager.start();
	}

	// Declarations for Locks
	static Object Check_MOPLock = new Object();

	// Declarations for Indexing Trees
	static javamoprt.MOPMap Check_sl_Map = new javamoprt.MOPMapOfMonitor(0);
	static Object Check_sl_Map_cachekey_0 = null;
	static Object Check_sl_Map_cachevalue = null;

	pointcut Check_getUser(ServletLogin sl) : (call(* ServletLogin.getUser(..)) && target(sl)) && !within(javamoprt.MOPObject+) && !adviceexecution();
	after (ServletLogin sl) : Check_getUser(sl) {
		Object obj = null;
		javamoprt.MOPMap m;
		CheckMonitor monitor = null;
		CheckMonitor_Set monitors = null;
		javamoprt.MOPWeakReference TempRef_sl;

		synchronized(Check_MOPLock) {
			if(sl == Check_sl_Map_cachekey_0){
				obj = Check_sl_Map_cachevalue;
			}

			if(obj == null) {
				obj = Check_sl_Map.get(sl);

				monitor = (CheckMonitor) obj;
				if (monitor == null){
					monitor = new CheckMonitor();
					monitor.MOPRef_sl = new javamoprt.MOPWeakReference(sl);
					Check_sl_Map.put(monitor.MOPRef_sl, monitor);
				}
				Check_sl_Map_cachekey_0 = sl;
				Check_sl_Map_cachevalue = monitor;
			} else {
				monitor = (CheckMonitor) obj;
			}
			monitor.Prop_1_event_getUser(sl);
			if(monitor.Prop_1_Category_fail) {
				monitor.Prop_1_handler_fail(sl);
			}
		}
	}

	pointcut Check_check(ServletLogin sl) : (call(* ServletLogin.check(..)) && target(sl)) && !within(javamoprt.MOPObject+) && !adviceexecution();
	before (ServletLogin sl) : Check_check(sl) {
		Object obj = null;
		javamoprt.MOPMap m;
		CheckMonitor monitor = null;
		CheckMonitor_Set monitors = null;
		javamoprt.MOPWeakReference TempRef_sl;

		synchronized(Check_MOPLock) {
			if(sl == Check_sl_Map_cachekey_0){
				obj = Check_sl_Map_cachevalue;
			}

			if(obj == null) {
				obj = Check_sl_Map.get(sl);

				monitor = (CheckMonitor) obj;
				if (monitor == null){
					monitor = new CheckMonitor();
					monitor.MOPRef_sl = new javamoprt.MOPWeakReference(sl);
					Check_sl_Map.put(monitor.MOPRef_sl, monitor);
				}
				Check_sl_Map_cachekey_0 = sl;
				Check_sl_Map_cachevalue = monitor;
			} else {
				monitor = (CheckMonitor) obj;
			}
			monitor.Prop_1_event_check(sl);
			if(monitor.Prop_1_Category_fail) {
				monitor.Prop_1_handler_fail(sl);
			}
		}
	}

}

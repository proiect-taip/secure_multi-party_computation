package base;

public class User {

	private String name;
	private String pass;
	private String isAdmin;

	public String getName()
	{
		return this.name;
	}

	public String getPass()
	{
		return this.pass;
	}

	public String getAdminSts()
	{
		return this.isAdmin;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public void setAdminSts(String isAdmin) {
		this.isAdmin = isAdmin;
	}

	@Override
	public String toString(){
		return "User: " + this.name +"\nPass: "+this.pass+"\nIs Admin: "+this.isAdmin+"\n";
	}

}

package base;

import java.util.*;
import javamoprt.*;

import java.io.FileWriter;
import java.lang.ref.*;
import org.aspectj.lang.*;

class FileVerifierMonitor_Set implements javamoprt.MOPSet {
	protected FileVerifierMonitor[] elementData;
	public int size;

	public FileVerifierMonitor_Set(){
		this.size = 0;
		this.elementData = new FileVerifierMonitor[4];
	}

	public final int size(){
		while(size > 0 && elementData[size-1].MOP_terminated) {
			elementData[--size] = null;
		}
		return size;
	}

	public final boolean add(MOPMonitor e){
		ensureCapacity();
		elementData[size++] = (FileVerifierMonitor)e;
		return true;
	}

	public final void endObject(int idnum){
		for(int i = 0; i < size; i++){
			MOPMonitor monitor = elementData[i];
			if(!monitor.MOP_terminated){
				monitor.endObject(idnum);
			}
		}
	}

	public final boolean alive(){
		for(int i = 0; i < size; i++){
			MOPMonitor monitor = elementData[i];
			if(!monitor.MOP_terminated){
				return true;
			}
		}
		return false;
	}

	public final void endObjectAndClean(int idnum){
		for(int i = size - 1; i > 0; i--){
			MOPMonitor monitor = elementData[i];
			if(monitor != null && !monitor.MOP_terminated){
				monitor.endObject(idnum);
			}
			elementData[i] = null;
		}
		elementData = null;
	}

	public final void ensureCapacity() {
		int oldCapacity = elementData.length;
		if (size + 1 > oldCapacity) {
			cleanup();
		}
		if (size + 1 > oldCapacity) {
			Object oldData[] = elementData;
			int newCapacity = (oldCapacity * 3) / 2 + 1;
			if (newCapacity < size + 1){
				newCapacity = size + 1;
			}
			elementData = Arrays.copyOf(elementData, newCapacity);
		}
	}

	public final void cleanup() {
		int num_terminated_monitors = 0 ;
		for(int i = 0; i + num_terminated_monitors < size; i ++){
			FileVerifierMonitor monitor = (FileVerifierMonitor)elementData[i + num_terminated_monitors];
			if(monitor.MOP_terminated){
				if(i + num_terminated_monitors + 1 < size){
					do{
						monitor = (FileVerifierMonitor)elementData[i + (++num_terminated_monitors)];
					} while(monitor.MOP_terminated && i + num_terminated_monitors + 1 < size);
					if(monitor.MOP_terminated){
						num_terminated_monitors++;
						break;
					}
				} else {
					num_terminated_monitors++;
					break;
				}
			}
			if(num_terminated_monitors != 0){
				elementData[i] = monitor;
			}
		}
		if(num_terminated_monitors != 0){
			size -= num_terminated_monitors;
			for(int i = size; i < size + num_terminated_monitors ; i++){
				elementData[i] = null;
			}
		}
	}

	public final void event_open(FileWriter f) {
		int num_terminated_monitors = 0 ;
		for(int i = 0; i + num_terminated_monitors < this.size; i ++){
			FileVerifierMonitor monitor = (FileVerifierMonitor)this.elementData[i + num_terminated_monitors];
			if(monitor.MOP_terminated){
				if(i + num_terminated_monitors + 1 < this.size){
					do{
						monitor = (FileVerifierMonitor)this.elementData[i + (++num_terminated_monitors)];
					} while(monitor.MOP_terminated && i + num_terminated_monitors + 1 < this.size);
					if(monitor.MOP_terminated){
						num_terminated_monitors++;
						break;
					}
				} else {
					num_terminated_monitors++;
					break;
				}
			}
			if(num_terminated_monitors != 0){
				this.elementData[i] = monitor;
			}
			monitor.Prop_1_event_open(f);
			if(monitor.Prop_1_Category_violation) {
				monitor.Prop_1_handler_violation(f);
			}
		}
		if(num_terminated_monitors != 0){
			this.size -= num_terminated_monitors;
			for(int i = this.size; i < this.size + num_terminated_monitors; i++){
				this.elementData[i] = null;
			}
		}
	}

	public final void event_write(FileWriter f) {
		int num_terminated_monitors = 0 ;
		for(int i = 0; i + num_terminated_monitors < this.size; i ++){
			FileVerifierMonitor monitor = (FileVerifierMonitor)this.elementData[i + num_terminated_monitors];
			if(monitor.MOP_terminated){
				if(i + num_terminated_monitors + 1 < this.size){
					do{
						monitor = (FileVerifierMonitor)this.elementData[i + (++num_terminated_monitors)];
					} while(monitor.MOP_terminated && i + num_terminated_monitors + 1 < this.size);
					if(monitor.MOP_terminated){
						num_terminated_monitors++;
						break;
					}
				} else {
					num_terminated_monitors++;
					break;
				}
			}
			if(num_terminated_monitors != 0){
				this.elementData[i] = monitor;
			}
			monitor.Prop_1_event_write(f);
			if(monitor.Prop_1_Category_violation) {
				monitor.Prop_1_handler_violation(f);
			}
		}
		if(num_terminated_monitors != 0){
			this.size -= num_terminated_monitors;
			for(int i = this.size; i < this.size + num_terminated_monitors; i++){
				this.elementData[i] = null;
			}
		}
	}

	public final void event_close(FileWriter f) {
		int num_terminated_monitors = 0 ;
		for(int i = 0; i + num_terminated_monitors < this.size; i ++){
			FileVerifierMonitor monitor = (FileVerifierMonitor)this.elementData[i + num_terminated_monitors];
			if(monitor.MOP_terminated){
				if(i + num_terminated_monitors + 1 < this.size){
					do{
						monitor = (FileVerifierMonitor)this.elementData[i + (++num_terminated_monitors)];
					} while(monitor.MOP_terminated && i + num_terminated_monitors + 1 < this.size);
					if(monitor.MOP_terminated){
						num_terminated_monitors++;
						break;
					}
				} else {
					num_terminated_monitors++;
					break;
				}
			}
			if(num_terminated_monitors != 0){
				this.elementData[i] = monitor;
			}
			monitor.Prop_1_event_close(f);
			if(monitor.Prop_1_Category_violation) {
				monitor.Prop_1_handler_violation(f);
			}
		}
		if(num_terminated_monitors != 0){
			this.size -= num_terminated_monitors;
			for(int i = this.size; i < this.size + num_terminated_monitors; i++){
				this.elementData[i] = null;
			}
		}
	}
}

class FileVerifierMonitor extends javamoprt.MOPMonitor implements Cloneable, javamoprt.MOPObject {
	public Object clone() {
		try {
			FileVerifierMonitor ret = (FileVerifierMonitor) super.clone();
			return ret;
		}
		catch (CloneNotSupportedException e) {
			throw new InternalError(e.toString());
		}
	}
	int x = 0;

	int Prop_1_state;
	static final int Prop_1_transition_open[] = {2, 3, 2, 3};;
	static final int Prop_1_transition_write[] = {1, 3, 2, 3};;
	static final int Prop_1_transition_close[] = {0, 3, 0, 3};;

	boolean Prop_1_Category_violation = false;

	public FileVerifierMonitor () {
		Prop_1_state = 0;

	}

	public final void Prop_1_event_open(FileWriter f) {
		MOP_lastevent = 0;

		Prop_1_state = Prop_1_transition_open[Prop_1_state];
		Prop_1_Category_violation = Prop_1_state == 1;
		{
			this.x = 0;
		}
	}

	public final void Prop_1_event_write(FileWriter f) {
		MOP_lastevent = 1;

		Prop_1_state = Prop_1_transition_write[Prop_1_state];
		Prop_1_Category_violation = Prop_1_state == 1;
		{
			this.x++;
		}
	}

	public final void Prop_1_event_close(FileWriter f) {
		MOP_lastevent = 2;

		Prop_1_state = Prop_1_transition_close[Prop_1_state];
		Prop_1_Category_violation = Prop_1_state == 1;
	}

	public final void Prop_1_handler_violation (FileWriter f){
		{
			System.out.println("Error: write() has been called after close()");
		}

	}

	public final void reset() {
		MOP_lastevent = -1;
		Prop_1_state = 0;
		Prop_1_Category_violation = false;
	}

	public javamoprt.MOPWeakReference MOPRef_f;

	//alive_parameters_0 = [FileWriter f]
	public boolean alive_parameters_0 = true;

	public final void endObject(int idnum){
		switch(idnum){
			case 0:
			alive_parameters_0 = false;
			break;
		}
		switch(MOP_lastevent) {
			case -1:
			return;
			case 0:
			//open
			//alive_f
			if(!(alive_parameters_0)){
				MOP_terminated = true;
				return;
			}
			break;

			case 1:
			//write
			//alive_f
			if(!(alive_parameters_0)){
				MOP_terminated = true;
				return;
			}
			break;

			case 2:
			//close
			//alive_f
			if(!(alive_parameters_0)){
				MOP_terminated = true;
				return;
			}
			break;

		}
		return;
	}

}

public aspect FileVerifierMonitorAspect implements javamoprt.MOPObject {
	javamoprt.MOPMapManager FileVerifierMapManager;
	public FileVerifierMonitorAspect(){
		FileVerifierMapManager = new javamoprt.MOPMapManager();
		FileVerifierMapManager.start();
	}

	// Declarations for Locks
	static Object FileVerifier_MOPLock = new Object();

	// Declarations for Indexing Trees
	static javamoprt.MOPMap FileVerifier_f_Map = new javamoprt.MOPMapOfMonitor(0);
	static Object FileVerifier_f_Map_cachekey_0 = null;
	static Object FileVerifier_f_Map_cachevalue = null;

	pointcut FileVerifier_open() : (call(FileWriter.new(..))) && !within(javamoprt.MOPObject+) && !adviceexecution();
	after () returning (FileWriter f) : FileVerifier_open() {
		Object obj = null;
		javamoprt.MOPMap m;
		FileVerifierMonitor monitor = null;
		FileVerifierMonitor_Set monitors = null;
		javamoprt.MOPWeakReference TempRef_f;

		synchronized(FileVerifier_MOPLock) {
			if(f == FileVerifier_f_Map_cachekey_0){
				obj = FileVerifier_f_Map_cachevalue;
			}

			if(obj == null) {
				obj = FileVerifier_f_Map.get(f);

				monitor = (FileVerifierMonitor) obj;
				if (monitor == null){
					monitor = new FileVerifierMonitor();
					monitor.MOPRef_f = new javamoprt.MOPWeakReference(f);
					FileVerifier_f_Map.put(monitor.MOPRef_f, monitor);
				}
				FileVerifier_f_Map_cachekey_0 = f;
				FileVerifier_f_Map_cachevalue = monitor;
			} else {
				monitor = (FileVerifierMonitor) obj;
			}
			monitor.Prop_1_event_open(f);
			if(monitor.Prop_1_Category_violation) {
				monitor.Prop_1_handler_violation(f);
			}
		}
	}

	pointcut FileVerifier_write(FileWriter f) : (call(* write(..)) && target(f)) && !within(javamoprt.MOPObject+) && !adviceexecution();
	before (FileWriter f) : FileVerifier_write(f) {
		Object obj = null;
		javamoprt.MOPMap m;
		FileVerifierMonitor monitor = null;
		FileVerifierMonitor_Set monitors = null;
		javamoprt.MOPWeakReference TempRef_f;

		synchronized(FileVerifier_MOPLock) {
			if(f == FileVerifier_f_Map_cachekey_0){
				obj = FileVerifier_f_Map_cachevalue;
			}

			if(obj == null) {
				obj = FileVerifier_f_Map.get(f);

				monitor = (FileVerifierMonitor) obj;
				if (monitor == null){
					monitor = new FileVerifierMonitor();
					monitor.MOPRef_f = new javamoprt.MOPWeakReference(f);
					FileVerifier_f_Map.put(monitor.MOPRef_f, monitor);
				}
				FileVerifier_f_Map_cachekey_0 = f;
				FileVerifier_f_Map_cachevalue = monitor;
			} else {
				monitor = (FileVerifierMonitor) obj;
			}
			monitor.Prop_1_event_write(f);
			if(monitor.Prop_1_Category_violation) {
				monitor.Prop_1_handler_violation(f);
			}
		}
	}

	pointcut FileVerifier_close(FileWriter f) : (call(* close(..)) && target(f)) && !within(javamoprt.MOPObject+) && !adviceexecution();
	after (FileWriter f) : FileVerifier_close(f) {
		Object obj = null;
		javamoprt.MOPMap m;
		FileVerifierMonitor monitor = null;
		FileVerifierMonitor_Set monitors = null;
		javamoprt.MOPWeakReference TempRef_f;

		synchronized(FileVerifier_MOPLock) {
			if(f == FileVerifier_f_Map_cachekey_0){
				obj = FileVerifier_f_Map_cachevalue;
			}

			if(obj == null) {
				obj = FileVerifier_f_Map.get(f);

				monitor = (FileVerifierMonitor) obj;
				if (monitor == null){
					monitor = new FileVerifierMonitor();
					monitor.MOPRef_f = new javamoprt.MOPWeakReference(f);
					FileVerifier_f_Map.put(monitor.MOPRef_f, monitor);
				}
				FileVerifier_f_Map_cachekey_0 = f;
				FileVerifier_f_Map_cachevalue = monitor;
			} else {
				monitor = (FileVerifierMonitor) obj;
			}
			monitor.Prop_1_event_close(f);
			if(monitor.Prop_1_Category_violation) {
				monitor.Prop_1_handler_violation(f);
			}
		}
	}

}

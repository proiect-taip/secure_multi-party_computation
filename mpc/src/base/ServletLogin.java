package base;

import java.io.IOException;
import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ServletLogin extends HttpServlet {

	private static final long serialVersionUID = 1L;
	static String ip;


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String name = request.getParameter("name");
		String pass = request.getParameter("pass");
		
		User u = null;

		 u = getUser(request, name);
		check(request,response,name,pass,u);
		

	}
	
	public User getUser(HttpServletRequest request, String name)
	{
		User u = new User();
		DB db = new DB();
		db.getUserByName(name,u);
		db.close();
		
		ip = getClientIpAddress(request);
		
		return u;
	}

	public String check(HttpServletRequest request, HttpServletResponse response, String name,String pass,User u) throws ServletException, IOException
	{


		if (name != null && pass != null) {

			if (u.getName()!=null && Crypt.validatePassword(pass, u.getPass()) == true && name.equals(u.getName())==true) {
				doSession(request, response);
			}
			else
			{
				request.setAttribute("err1", String.format("Debug: username %s and the password %s!", name,pass));
				request.getRequestDispatcher("/index.jsp").forward(request, response);
			}
		}

		return u.getName();
	}



	public void doSession(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

		HttpSession session = request.getSession(true);
		response.setContentType("text/html");
		Integer accessCount = new Integer(0);
		if (!session.isNew()) {
			Integer oldAccessCount = (Integer)session.getAttribute("accessCount"); 
			if (oldAccessCount != null) {
				accessCount = new Integer(oldAccessCount.intValue() + 1);
			}
		}

		String json = "[['Diseases', 'Number of people'],['Cancer',     11],['HIV',      2],['ADHD',  2],['Diabetes', 4]]";

		session.setAttribute("accessCount", accessCount); 
		session.setAttribute("chart", json);
		request.getRequestDispatcher("/home.jsp").forward(request, response);

	}

	public static String getClientIpAddress(HttpServletRequest request) {
		String[] HEADERS_TO_TRY = { 
				"X-Forwarded-For",
				"Proxy-Client-IP",
				"WL-Proxy-Client-IP",
				"HTTP_X_FORWARDED_FOR",
				"HTTP_X_FORWARDED",
				"HTTP_X_CLUSTER_CLIENT_IP",
				"HTTP_CLIENT_IP",
				"HTTP_FORWARDED_FOR",
				"HTTP_FORWARDED",
				"HTTP_VIA",
		"REMOTE_ADDR" };
		
		for (String header : HEADERS_TO_TRY) {
			String ip = request.getHeader(header);
			if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
				return ip;
			}
		}
		return request.getRemoteAddr();
	}
	

}
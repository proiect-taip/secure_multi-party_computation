package base;

import java.io.File;
import java.io.FileWriter;

public class FileVerifierTest {
	public static void main(String[] args){
		FileWriter filewriter1=null;
		FileWriter filewriter2=null;
		FileWriter filewriter3=null;
		try{
			filewriter1 = new FileWriter(File.createTempFile("test1", ".tmp"));
			filewriter2 = new FileWriter(File.createTempFile("test2", ".tmp"));
			filewriter3 = new FileWriter(File.createTempFile("test3", ".tmp"));

			filewriter1.write("aaa");
			filewriter2.write("aaa");
			filewriter3.write("aaa");
			filewriter1.write("aaa");
			filewriter3.write("aaa");

			filewriter1.close();
			filewriter2.close();
			filewriter3.close();

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		try{
			filewriter1.write("aaa");
		} catch (Exception e) {
		}
		try{
			filewriter2.write("aaa");
		} catch (Exception e) {
		}
	}
}




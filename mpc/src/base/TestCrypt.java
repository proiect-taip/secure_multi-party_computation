package base;

import junit.framework.TestCase;

public class TestCrypt extends TestCase {

	public void testGenValid()
	{
		String orgPass = "";
		boolean bool = false;
		orgPass = Crypt.generateStrongPasswordHash("abc");
		bool = Crypt.validatePassword("abc", orgPass);

		assertTrue(bool);
	}

	public void testSalt()
	{
		String pass1 = null, pass2 = null, pass3 = null;
		pass1 = Crypt.generateStrongPasswordHash("abc");
		pass2 = Crypt.generateStrongPasswordHash("abc");
		pass3 = Crypt.generateStrongPasswordHash("abc");

		assertFalse(pass1.equals(pass2) && pass1.equals(pass3) && pass3.equals(pass2));
	}

}

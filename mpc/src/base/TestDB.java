package base;

import junit.framework.TestCase;

public class TestDB extends TestCase {

	public void testReg()
	{
		DB db = new DB();

		User uOrg = new User();
		User uNew = new User();

		uOrg.setName("TestName1");
		uOrg.setPass(Crypt.generateStrongPasswordHash("pass1"));
		uOrg.setAdminSts("false");
		db.insertUser(uOrg);

		db.getUserByName(uOrg.getName(), uNew);

		assertEquals(uOrg.getName(), uNew.getName());
	}

	public void testSel()
	{
		DB db = new DB();

		User uNew = new User();

		db.getUserByName("TestName", uNew);

		assertEquals("false", uNew.getAdminSts());
		assertEquals("5b42403364373164353532,88cc02e9b48bc60fb4cd21ac3798969cbbe20862a161e0b045b71f11c2fb849cfa32ad5863b3a19b60b5f98f6fc7a05bd8a280ca84fb3cef7fc98097ff31cc58", uNew.getPass());
		assertEquals("TestName", uNew.getName());
	}


}

package base;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public aspect DBAspect {
	

	pointcut action2(): execution( protected void ServletRegister.doPost(..));
	void around()  :action2()
	{
		
		DB db = new DB();
		User u = new User();

		Object request = thisJoinPoint.getArgs()[0];
		Object response = thisJoinPoint.getArgs()[1];
		String s = ((HttpServletRequest)request).getParameter("name");
		

		try {
			db.getUserByName(s, u);
			if (u.getName() != null) {
				Log.write("User "+ s +" already exists\n");
				((HttpServletRequest)request).setAttribute("err2", "The username you just typed in already exists !");
				try {
					((HttpServletRequest)request).getRequestDispatcher("/index.jsp").forward(((HttpServletRequest)request),  (HttpServletResponse)response);
				} catch (ServletException | IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else {
				Log.write("User "+ s +"  registered\n");
				proceed();
			}

		} catch (SQLException | java.lang.NullPointerException e) {
			Log.write("User "+ s +"  registered\n");
			proceed();
		}

		//db.close();
	}

}

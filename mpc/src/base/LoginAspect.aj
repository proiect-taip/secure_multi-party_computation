package base;

import java.io.IOException;

public aspect LoginAspect {
	pointcut action(): execution( public String ServletLogin.check(..));
	before(): action()
	{
		Log.write("IP "+ServletLogin.ip+" trying to login\n");

	}

	pointcut afterCheck(): execution(public String ServletLogin.check(..));
	after() returning (String s) throws IOException: afterCheck() {
		Log.write("User "+ s +" logged in\n");
	}
	after() throwing (Exception e): afterCheck() {
		System.out.println("Threw an exception: " + e);
	}


}

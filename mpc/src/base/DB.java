package base;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;



public class DB {

	private  Connection conn = null;
	private  String url = "jdbc:mysql://localhost:3306/mpc";
	private String user = "root";
	private String password = "dragos123";

	public void close()
	{
		try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		Log.write("Disconnected from database\n");
	}

	public DB() {

		try {

			Class.forName("com.mysql.jdbc.Driver").newInstance();
			conn = DriverManager.getConnection(url, user, password);

		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException | SQLException e1) {
			e1.printStackTrace();
		}

		if (conn != null) {
			Log.write("Connected to database\n");
		}
		else
			Log.write("Connection failed\n");
	}


	public void getUserByName(String name, User u)
	{

		try {
			String query = "SELECT name, pass, isAdmin FROM users WHERE name = ?";    
			PreparedStatement prest;
			prest = conn.prepareStatement(query);
			prest.setString(1, name);
			ResultSet rs = prest.executeQuery();

			while(rs.next()) {
				u.setName(rs.getString("name"));
				u.setPass(rs.getString("pass"));
				u.setAdminSts(rs.getString("isAdmin"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		Log.write("SQL ANALASYS:\nSelect returned:\n"+u.toString());
	}

	public void insertUser(User u)
	{

		try {

			String query = "insert into users (name, pass, isAdmin) values (?, ?, ?)";
			PreparedStatement preparedStmt;
			preparedStmt = conn.prepareStatement(query);
			preparedStmt.setString (1, u.getName());
			preparedStmt.setString (2, u.getPass() );
			preparedStmt.setString(3, u.getAdminSts());

			preparedStmt.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		}


		Log.write("SQL ANALASYS:\nInserted user:\n"+u.toString());
	}

}

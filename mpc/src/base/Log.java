package base;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Log {

	public static void write(String s) {
		FileWriter f;
		try {
			f = new FileWriter(new File("D:\\Programs\\Eclipse\\ServletMPC\\Log\\log"),true);
			f.write("["+new SimpleDateFormat("yyyy-MM-dd, HH:mm:ss").format(Calendar.getInstance().getTime())+"] "+s);
			f.flush();
			f.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}

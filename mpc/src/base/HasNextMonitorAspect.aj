package base;

import java.io.*;
import java.util.*;
import javamoprt.*;
import java.lang.ref.*;
import org.aspectj.lang.*;

class HasNextSuffixMonitor_Set implements javamoprt.MOPSet {
	protected HasNextSuffixMonitor[] elementData;
	public int size;

	public HasNextSuffixMonitor_Set(){
		this.size = 0;
		this.elementData = new HasNextSuffixMonitor[4];
	}

	public final int size(){
		while(size > 0 && elementData[size-1].MOP_terminated) {
			elementData[--size] = null;
		}
		return size;
	}

	public final boolean add(MOPMonitor e){
		ensureCapacity();
		elementData[size++] = (HasNextSuffixMonitor)e;
		return true;
	}

	public final void endObject(int idnum){
		for(int i = 0; i < size; i++){
			MOPMonitor monitor = elementData[i];
			if(!monitor.MOP_terminated){
				monitor.endObject(idnum);
			}
		}
	}

	public final boolean alive(){
		for(int i = 0; i < size; i++){
			MOPMonitor monitor = elementData[i];
			if(!monitor.MOP_terminated){
				return true;
			}
		}
		return false;
	}

	public final void endObjectAndClean(int idnum){
		for(int i = size - 1; i > 0; i--){
			MOPMonitor monitor = elementData[i];
			if(monitor != null && !monitor.MOP_terminated){
				monitor.endObject(idnum);
			}
			elementData[i] = null;
		}
		elementData = null;
	}

	public final void ensureCapacity() {
		int oldCapacity = elementData.length;
		if (size + 1 > oldCapacity) {
			cleanup();
		}
		if (size + 1 > oldCapacity) {
			Object oldData[] = elementData;
			int newCapacity = (oldCapacity * 3) / 2 + 1;
			if (newCapacity < size + 1){
				newCapacity = size + 1;
			}
			elementData = Arrays.copyOf(elementData, newCapacity);
		}
	}

	public final void cleanup() {
		int num_terminated_monitors = 0 ;
		for(int i = 0; i + num_terminated_monitors < size; i ++){
			HasNextSuffixMonitor monitor = (HasNextSuffixMonitor)elementData[i + num_terminated_monitors];
			if(monitor.MOP_terminated){
				if(i + num_terminated_monitors + 1 < size){
					do{
						monitor = (HasNextSuffixMonitor)elementData[i + (++num_terminated_monitors)];
					} while(monitor.MOP_terminated && i + num_terminated_monitors + 1 < size);
					if(monitor.MOP_terminated){
						num_terminated_monitors++;
						break;
					}
				} else {
					num_terminated_monitors++;
					break;
				}
			}
			if(num_terminated_monitors != 0){
				elementData[i] = monitor;
			}
		}
		if(num_terminated_monitors != 0){
			size -= num_terminated_monitors;
			for(int i = size; i < size + num_terminated_monitors ; i++){
				elementData[i] = null;
			}
		}
	}

	public final void event_hasnext(Iterator i) {
		int num_terminated_monitors = 0 ;
		for(int i_1 = 0; i_1 + num_terminated_monitors < this.size; i_1 ++){
			HasNextSuffixMonitor monitor = (HasNextSuffixMonitor)this.elementData[i_1 + num_terminated_monitors];
			if(monitor.MOP_terminated){
				if(i_1 + num_terminated_monitors + 1 < this.size){
					do{
						monitor = (HasNextSuffixMonitor)this.elementData[i_1 + (++num_terminated_monitors)];
					} while(monitor.MOP_terminated && i_1 + num_terminated_monitors + 1 < this.size);
					if(monitor.MOP_terminated){
						num_terminated_monitors++;
						break;
					}
				} else {
					num_terminated_monitors++;
					break;
				}
			}
			if(num_terminated_monitors != 0){
				this.elementData[i_1] = monitor;
			}
			monitor.event_hasnext(i);
		}
		if(num_terminated_monitors != 0){
			this.size -= num_terminated_monitors;
			for(int i_1 = this.size; i_1 < this.size + num_terminated_monitors; i_1++){
				this.elementData[i_1] = null;
			}
		}
	}

	public final void event_next(Iterator i) {
		int num_terminated_monitors = 0 ;
		for(int i_1 = 0; i_1 + num_terminated_monitors < this.size; i_1 ++){
			HasNextSuffixMonitor monitor = (HasNextSuffixMonitor)this.elementData[i_1 + num_terminated_monitors];
			if(monitor.MOP_terminated){
				if(i_1 + num_terminated_monitors + 1 < this.size){
					do{
						monitor = (HasNextSuffixMonitor)this.elementData[i_1 + (++num_terminated_monitors)];
					} while(monitor.MOP_terminated && i_1 + num_terminated_monitors + 1 < this.size);
					if(monitor.MOP_terminated){
						num_terminated_monitors++;
						break;
					}
				} else {
					num_terminated_monitors++;
					break;
				}
			}
			if(num_terminated_monitors != 0){
				this.elementData[i_1] = monitor;
			}
			monitor.event_next(i);
		}
		if(num_terminated_monitors != 0){
			this.size -= num_terminated_monitors;
			for(int i_1 = this.size; i_1 < this.size + num_terminated_monitors; i_1++){
				this.elementData[i_1] = null;
			}
		}
	}
}

class HasNextSuffixMonitor extends javamoprt.MOPMonitor implements Cloneable, javamoprt.MOPObject {
	Vector<HasNextMonitor> monitorList = new Vector<HasNextMonitor>();
	public Object clone() {
		try {
			HasNextSuffixMonitor ret = (HasNextSuffixMonitor) super.clone();
			ret.monitorList = new Vector<HasNextMonitor>();
			for(HasNextMonitor monitor : this.monitorList){
				HasNextMonitor newMonitor = (HasNextMonitor)monitor.clone();
				ret.monitorList.add(newMonitor);
			}
			return ret;
		}
		catch (CloneNotSupportedException e) {
			throw new InternalError(e.toString());
		}
	}

	public final void event_hasnext(Iterator i) {
		MOP_lastevent = 0;
		HashSet monitorSet = new HashSet();
		HasNextMonitor newMonitor = new HasNextMonitor();
		monitorList.add(newMonitor);
		Iterator it = monitorList.iterator();
		while (it.hasNext()){
			HasNextMonitor monitor = (HasNextMonitor)it.next();
			monitor.Prop_1_event_hasnext(i);
			if(monitor.Prop_1_Category_match) {
				monitor.Prop_1_handler_match(i);
			}
			if(monitorSet.contains(monitor) || monitor.Prop_1_Category_match ) {
				it.remove();
			} else {
				monitorSet.add(monitor);
			}
		}
	}

	public final void event_next(Iterator i) {
		MOP_lastevent = 1;
		HashSet monitorSet = new HashSet();
		HasNextMonitor newMonitor = new HasNextMonitor();
		monitorList.add(newMonitor);
		Iterator it = monitorList.iterator();
		while (it.hasNext()){
			HasNextMonitor monitor = (HasNextMonitor)it.next();
			monitor.Prop_1_event_next(i);
			if(monitor.Prop_1_Category_match) {
				monitor.Prop_1_handler_match(i);
			}
			if(monitorSet.contains(monitor) || monitor.Prop_1_Category_match ) {
				it.remove();
			} else {
				monitorSet.add(monitor);
			}
		}
	}

	public javamoprt.MOPWeakReference MOPRef_i;

	public final void endObject(int idnum){
		switch(idnum){
			case 0:
			break;
		}
		switch(MOP_lastevent) {
			case -1:
			return;
			case 0:
			//hasnext
			return;
			case 1:
			//next
			return;
		}
		return;
	}

}

class HasNextMonitor implements Cloneable, javamoprt.MOPObject {
	public Object clone() {
		try {
			HasNextMonitor ret = (HasNextMonitor) super.clone();
			ret.Prop_1_stacks = new ArrayList<IntStack>();
			for(int Prop_1_i = 0; Prop_1_i < this.Prop_1_stacks.size(); Prop_1_i++){
				IntStack Prop_1_stack = this.Prop_1_stacks.get(Prop_1_i);
				ret.Prop_1_stacks.add(Prop_1_stack.fclone());
			}
			return ret;
		}
		catch (CloneNotSupportedException e) {
			throw new InternalError(e.toString());
		}
	}

	/* %%_%_CFG_%_%% */ArrayList<IntStack> Prop_1_stacks = new ArrayList<IntStack>();
	static int[][] Prop_1_gt = { { 0, -1,  }, { 0, -1,  }, { 0, 1,  }, { 0, -1,  },  };;
	static int[][][][] Prop_1_at = { { {  }, {  },  }, { {  }, {  },  }, { {  }, { { 3,  },  },  }, { {  }, { { 0,  },  },  },  };;
	static boolean[] Prop_1_acc = { true, true, false, false, };;
	int Prop_1_cat; // ACCEPT = 0, UNKNOWN = 1, FAIL = 2
	int Prop_1_event = -1;
	class IntStack implements java.io.Serializable {
		int[] data;
		int curr_index = 0;
		public IntStack(){
			data = new int[32];
		}
		public String toString(){
			String ret = "[";
			for (int i = curr_index; i>=0; i--){
				ret += Integer.toString(data[i])+",";
			}
			return ret+"]";
		}
		public int hashCode() {
			return curr_index^peek();
		}
		public boolean equals(Object o) {
			if (o == null) return false;
			if (!(o instanceof IntStack)) return false;
			IntStack s = (IntStack)o;
			if(curr_index != s.curr_index) return false;
			for(int i = 0; i < curr_index; i++){
				if(data[i] != s.data[i]) return false;
			}
			return true;
		}
		public IntStack(int size){
			data = new int[size];
		}
		public int peek(){
			return data[curr_index - 1];
		}
		public int pop(){
			return data[--curr_index];
		}
		public void pop(int num){
			curr_index -= num;
		}
		public void push(int datum){
			if(curr_index < data.length){
				data[curr_index++] = datum;
			} else{
				int len = data.length;
				int[] old = data;
				data = new int[len * 2];
				for(int i = 0; i < len; ++i){
					data[i] = old[i];
				}
				data[curr_index++] = datum;
			}
		}
		public IntStack fclone(){
			IntStack ret = new IntStack(data.length);
			ret.curr_index = curr_index;
			for(int i = 0; i < curr_index; ++i){
				ret.data[i] = data[i];
			}
			return ret;
		}
		public void clear(){
			curr_index = 0;
		}
	}

	boolean Prop_1_Category_match = false;

	public HasNextMonitor () {
		IntStack stack = new IntStack();
		stack.push(-2);
		stack.push(2);
		Prop_1_stacks.add(stack);

	}

	public final void Prop_1_event_hasnext(Iterator i) {

		Prop_1_event = 1;
		if (Prop_1_cat != 2) {
			Prop_1_event--;
			Prop_1_cat = 1;
			for (int Prop_1_i = Prop_1_stacks.size()-1; Prop_1_i >=0; Prop_1_i--) {
				IntStack stack = Prop_1_stacks.get(Prop_1_i);
				Prop_1_stacks.set(Prop_1_i,null);
				while (stack != null) {
					int s = stack.peek();
					if (s >= 0 && Prop_1_at[s][Prop_1_event].length >= 0) {
						/* not in an error state and something to do? */
						for (int j = 0; j < Prop_1_at[s][Prop_1_event].length; j++) {
							IntStack tstack;
							if (Prop_1_at[s][Prop_1_event].length > 1){
								tstack = stack.fclone();
							} else{
								tstack = stack;
							}
							switch (Prop_1_at[s][Prop_1_event][j].length) {
								case 1:/* Shift */
								tstack.push(Prop_1_at[s][Prop_1_event][j][0]);
								Prop_1_stacks.add(tstack);
								if (Prop_1_acc[Prop_1_at[s][Prop_1_event][j][0]]) Prop_1_cat = 0;
								break;
								case 2: /* Reduce */
								tstack.pop(Prop_1_at[s][Prop_1_event][j][1]);
								int Prop_1_old = tstack.peek();
								tstack.push(Prop_1_gt[Prop_1_old][Prop_1_at[s][Prop_1_event][j][0]]);
								Prop_1_stacks.add(Prop_1_i,tstack);
								break;
							}
						}
					}
					stack = Prop_1_stacks.get(Prop_1_i);
					Prop_1_stacks.remove(Prop_1_i);
				}
			}
			if (Prop_1_stacks.isEmpty())
			Prop_1_cat = 2;
		}
		Prop_1_Category_match = Prop_1_cat == 0;
	}

	public final void Prop_1_event_next(Iterator i) {

		Prop_1_event = 2;
		if (Prop_1_cat != 2) {
			Prop_1_event--;
			Prop_1_cat = 1;
			for (int Prop_1_i = Prop_1_stacks.size()-1; Prop_1_i >=0; Prop_1_i--) {
				IntStack stack = Prop_1_stacks.get(Prop_1_i);
				Prop_1_stacks.set(Prop_1_i,null);
				while (stack != null) {
					int s = stack.peek();
					if (s >= 0 && Prop_1_at[s][Prop_1_event].length >= 0) {
						/* not in an error state and something to do? */
						for (int j = 0; j < Prop_1_at[s][Prop_1_event].length; j++) {
							IntStack tstack;
							if (Prop_1_at[s][Prop_1_event].length > 1){
								tstack = stack.fclone();
							} else{
								tstack = stack;
							}
							switch (Prop_1_at[s][Prop_1_event][j].length) {
								case 1:/* Shift */
								tstack.push(Prop_1_at[s][Prop_1_event][j][0]);
								Prop_1_stacks.add(tstack);
								if (Prop_1_acc[Prop_1_at[s][Prop_1_event][j][0]]) Prop_1_cat = 0;
								break;
								case 2: /* Reduce */
								tstack.pop(Prop_1_at[s][Prop_1_event][j][1]);
								int Prop_1_old = tstack.peek();
								tstack.push(Prop_1_gt[Prop_1_old][Prop_1_at[s][Prop_1_event][j][0]]);
								Prop_1_stacks.add(Prop_1_i,tstack);
								break;
							}
						}
					}
					stack = Prop_1_stacks.get(Prop_1_i);
					Prop_1_stacks.remove(Prop_1_i);
				}
			}
			if (Prop_1_stacks.isEmpty())
			Prop_1_cat = 2;
		}
		Prop_1_Category_match = Prop_1_cat == 0;
	}

	public final void Prop_1_handler_match (Iterator i){
		{
			System.err.println("! hasNext not called before next");
			this.reset();
		}

	}

	public final void reset() {
		Prop_1_stacks.clear();
		IntStack stack = new IntStack();
		stack.push(-2);
		stack.push(2);
		Prop_1_stacks.add(stack);
		Prop_1_Category_match = false;
	}

	public final int hashCode() {
		if(Prop_1_stacks.size() == 0) return 0;
		return Prop_1_stacks.size() ^ Prop_1_stacks.get(Prop_1_stacks.size() - 1).hashCode();
	}

	public final boolean equals(Object o) {
		if(o == null) return false;
		if(! (o instanceof HasNextMonitor)) return false ;
		HasNextMonitor m = (HasNextMonitor) o;
		if (Prop_1_stacks.size() != m.Prop_1_stacks.size()) return false;
		for(int Prop_1_i = 0; Prop_1_i < Prop_1_stacks.size(); Prop_1_i++){
			IntStack Prop_1_stack = Prop_1_stacks.get(Prop_1_i);
			IntStack Prop_1_stack2 = m.Prop_1_stacks.get(Prop_1_i);
			if(Prop_1_stack.curr_index != Prop_1_stack2.curr_index) return false;
			for(int Prop_1_j = 0; Prop_1_j < Prop_1_stack.curr_index; Prop_1_j++){
				if(Prop_1_stack.data[Prop_1_j] != Prop_1_stack2.data[Prop_1_j]) return false;
			}
		}
		return true;
	}

}

public aspect HasNextMonitorAspect implements javamoprt.MOPObject {
	javamoprt.MOPMapManager HasNextMapManager;
	public HasNextMonitorAspect(){
		HasNextMapManager = new javamoprt.MOPMapManager();
		HasNextMapManager.start();
	}

	// Declarations for Locks
	static Object HasNext_MOPLock = new Object();

	// Declarations for Indexing Trees
	static javamoprt.MOPMap HasNext_i_Map = new javamoprt.MOPMapOfMonitor(0);
	static Object HasNext_i_Map_cachekey_0 = null;
	static Object HasNext_i_Map_cachevalue = null;

	pointcut HasNext_hasnext(Iterator i) : (call(* Iterator.hasNext()) && target(i)) && !within(javamoprt.MOPObject+) && !adviceexecution();
	before (Iterator i) : HasNext_hasnext(i) {
		Object obj = null;
		javamoprt.MOPMap m;
		HasNextSuffixMonitor monitor = null;
		HasNextSuffixMonitor_Set monitors = null;
		javamoprt.MOPWeakReference TempRef_i;

		synchronized(HasNext_MOPLock) {
			if(i == HasNext_i_Map_cachekey_0){
				obj = HasNext_i_Map_cachevalue;
			}

			if(obj == null) {
				obj = HasNext_i_Map.get(i);

				monitor = (HasNextSuffixMonitor) obj;
				if (monitor == null){
					monitor = new HasNextSuffixMonitor();
					monitor.MOPRef_i = new javamoprt.MOPWeakReference(i);
					HasNext_i_Map.put(monitor.MOPRef_i, monitor);
				}
				HasNext_i_Map_cachekey_0 = i;
				HasNext_i_Map_cachevalue = monitor;
			} else {
				monitor = (HasNextSuffixMonitor) obj;
			}
			monitor.event_hasnext(i);
		}
	}

	pointcut HasNext_next(Iterator i) : (call(* Iterator.next()) && target(i)) && !within(javamoprt.MOPObject+) && !adviceexecution();
	before (Iterator i) : HasNext_next(i) {
		Object obj = null;
		javamoprt.MOPMap m;
		HasNextSuffixMonitor monitor = null;
		HasNextSuffixMonitor_Set monitors = null;
		javamoprt.MOPWeakReference TempRef_i;

		synchronized(HasNext_MOPLock) {
			if(i == HasNext_i_Map_cachekey_0){
				obj = HasNext_i_Map_cachevalue;
			}

			if(obj == null) {
				obj = HasNext_i_Map.get(i);

				monitor = (HasNextSuffixMonitor) obj;
				if (monitor == null){
					monitor = new HasNextSuffixMonitor();
					monitor.MOPRef_i = new javamoprt.MOPWeakReference(i);
					HasNext_i_Map.put(monitor.MOPRef_i, monitor);
				}
				HasNext_i_Map_cachekey_0 = i;
				HasNext_i_Map_cachevalue = monitor;
			} else {
				monitor = (HasNextSuffixMonitor) obj;
			}
			monitor.event_next(i);
		}
	}

}

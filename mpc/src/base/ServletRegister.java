package base;

import java.io.IOException;
import javax.servlet.ServletException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ServletRegister extends HttpServlet {

	private static final long serialVersionUID = 1L;
	static String ip;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		
		ip = getClientIpAddress(request);

		String name = request.getParameter("name");
		String pass = request.getParameter("pass");
		String admin = request.getParameter("admin");
		
		register(name,pass,admin);
		
		request.getRequestDispatcher("/success.jsp").forward(request, response);

	}
	
	public static void register(String name,String pass, String admin)
	{
		DB db = new DB();
		User u = new User();
		String status;

		if(admin.equals("Not Admin"))
			status = "false";
		else
			status = "true";


		u.setName(name);
		u.setAdminSts(status);
		u.setPass(Crypt.generateStrongPasswordHash(pass));
		db.insertUser(u);
		db.close();
	}
	
	public static String getClientIpAddress(HttpServletRequest request) {
		String[] HEADERS_TO_TRY = { 
				"X-Forwarded-For",
				"Proxy-Client-IP",
				"WL-Proxy-Client-IP",
				"HTTP_X_FORWARDED_FOR",
				"HTTP_X_FORWARDED",
				"HTTP_X_CLUSTER_CLIENT_IP",
				"HTTP_CLIENT_IP",
				"HTTP_FORWARDED_FOR",
				"HTTP_FORWARDED",
				"HTTP_VIA",
		"REMOTE_ADDR" };
		
		for (String header : HEADERS_TO_TRY) {
			String ip = request.getHeader(header);
			if (ip != null && ip.length() != 0 && !"unknown".equalsIgnoreCase(ip)) {
				return ip;
			}
		}
		return request.getRemoteAddr();
	}

}